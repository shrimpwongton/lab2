package edu.ucsd.cs110s.temperature;
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return "" + super.getValue() + " degrees F.";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		Temperature t = new Celsius( (float) ((super.getValue() -32) / 1.8) );
		return t;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		Temperature t = new Fahrenheit(super.getValue());
		return t;
	}
}
