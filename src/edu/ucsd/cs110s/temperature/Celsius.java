package edu.ucsd.cs110s.temperature;
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return "" + super.getValue() + " degrees C.";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		Temperature t = new Celsius(super.getValue());
		return t;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		Temperature t = new Fahrenheit((float) (super.getValue() * 1.8 + 32));
		return t;
	}
}
